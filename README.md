# Restore Online Freedom!
<!--
  Copyright (C) 2016 Mike Gerwitz

  This work is licensed under a Creative Commons Attribution-ShareAlike
  4.0 International License.

  You should have received a copy of the license along with this
  work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
-->

LaTeX sources for my talk "Restore Online Freedom!" at
[LibrePlanet 2016][lp2016].  The sources produce the slides; notes for the
talk are included in the sources throughout, but are not compiled in any
way.


## Description

Imagine a world where surveillance is the default and users must opt-in to
privacy.  Imagine that your every action is logged and analyzed to learn how
you behave, what your interests are, and what you might do next.  Imagine
that, even on your fully free operating system, proprietary software is
automatically downloaded and run not only without your consent, but often
without your knowledge.  In this world, even free software cannot be easily
modified, shared, or replaced.  In many cases, you might not even be in
control of your own computing---your actions and your data might be in
control by a remote entity, and only they decide what you are and are not
allowed to do.

This may sound dystopian, but this is the world you're living in right
now.  The Web today is an increasingly hostile, freedom-denying place that
propagates to nearly every aspect of the average users' lives---from their
PCs to their phones, to their TVs and beyond.  But before we can stand up
and demand back our freedoms, we must understand what we're being robbed of,
how it's being done, and what can (or can't) be done to stop it.

[lp2016]: https://www.libreplanet.org/2016/program/#day-2-timeslot-8-session-3
